package cn.weixin.song.contant;

/**
 * 常量定义
 * @author eason
 */
public final class R {
	
	/**
	 * 菜单引用类型（某个模块）
	 */
	public static final class MenuRefType{
		/**
		 * 活动
		 */
		public static final int activity= 1;
		/**
		 * 我的优惠券
		 */
		public static final int my_coupon= 2;
		
	}
	/**
	 * 菜单模块
	 * @author eason
	 *
	 */
	public static final class ModelType {
		 /**
		  * 默认模块
		  */
		  public static final Integer NONE      = 0;
		  /**
		   * 猜歌模块
		   */
		  public static final Integer MUSIC     = 1;
	}
	
}
