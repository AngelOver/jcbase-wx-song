<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="apple-touch-fullscreen" content="YES" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="viewport" content="width=device-width, initial-scale=1.0,  minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<title>优惠券核销</title>

<link href="${res_url}wxcj/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="http://libs.baidu.com/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript" src="${res_url}wx/res/layer/layer/layer.js"></script>

<script type="text/javascript">
var turnplate={
		restaraunts:[],				//大转盘奖品名称
		colors:[],					//大转盘奖品区块对应背景颜色
		outsideRadius:192,			//大转盘外圆的半径
		textRadius:155,				//大转盘奖品位置距离圆心的距离
		insideRadius:68,			//大转盘内圆的半径
		startAngle:0,				//开始角度
		
		bRotate:false				//false:停止;ture:旋转
};

$(document).ready(function(){
document.getElementById('coupon_name').innerHTML="${item.coupon_name}";
document.getElementById('gameWinId').style.opacity=1;
document.getElementById('gameWinId').style.webkitTransform='scale(1)';
if("${item.status}"=="0"){
document.getElementById('submitUserInfoId').onclick=function(){
	if($.trim($("#pwd").val())==""){
		layer.open({
		    content: '请输入核销密码',
		    time: 2
		});
		return;
	}
	var postData={sn:"${item.sn_code}",pwd:$("#pwd").val()};
    $.post("/wx/coupon/check", postData, function(rs){
      	 if (rs.code == 0) {
      			layer.open({
    			    content: '消费成功',
    			    time: 2
    			});
      			$("#submitUserInfoId").hide();
      			$("#pwd").hide();
      			$("#pwd_tips").html("消费成功");
               } else {
            	   layer.open({
           		    content: rs.msg,
           		    time: 2
           		});
               }
      }, 'json');
}
}
});

</script>
</head>
<body>
<div class="main" id="mainId">
	
    <div class="gameWin" id="gameWinId" style="margin-top: 20px;">
    	<div class="infoBox">
        	<h1 id="coupon_name" style="color: #df540e;"></h1>
            <div class="h10"></div>
              <c:if test="${item.status eq 0 }">
            <p class="f12" id="pwd_tips">输入优惠券核销密码</p>
             <div class="h5"></div>
           
            <p>
            	<input type="text" name="pwd" id="pwd" value="${check_pwd}" placeholder="填写核销密码" maxlength="11" />
            </p>
            </c:if>
             <c:if test="${item.status eq 1 }">
              <p>该券已消费过</p>
               <div class="h10"></div>
               <p>消费时间：<fmt:formatDate value="${item.used_time }" pattern="yyyy-MM-dd HH:mm:ss"/></p>
             </c:if>
            <div class="h10"></div>
             <c:if test="${item.status eq 0 }">
            <div class="infoBtn" id="submitUserInfoId">
            	确 定
            </div>
            </c:if>
            <div class="h10"></div>
        </div>
    </div>
    </div>
</body>
</html>
