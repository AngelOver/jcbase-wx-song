<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/taglib.jsp"%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Dashboard - Ace Admin</title>

		<meta name="description" content="overview &amp; stats" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="/res/ace-1.3.3/assets/css/bootstrap.css" />
		<link rel="stylesheet" href="/res/ace-1.3.3/assets/css/font-awesome.css" />

		<!-- page specific plugin styles -->
		<link rel="stylesheet" href="/res/ace-1.3.3/assets/css/jquery-ui.css" />
		<link rel="stylesheet" href="/res/ace-1.3.3/assets/css/datepicker.css" />
		<link rel="stylesheet" href="/res/ace-1.3.3/assets/css/ui.jqgrid.css" />

		<!-- text fonts -->
		<link rel="stylesheet" href="/res/ace-1.3.3/assets/css/ace-fonts.css" />

		<!-- ace styles -->
		<link rel="stylesheet" href="/res/ace-1.3.3/assets/css/ace.css" class="ace-main-stylesheet" id="main-ace-style" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="/res/ace-1.3.3/assets/css/ace-part2.css" class="ace-main-stylesheet" />
		<![endif]-->

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="/res/ace-1.3.3/assets/css/ace-ie.css" />
		<![endif]-->

		<!-- inline styles related to this page -->

		<!-- ace settings handler -->
		<script src="/res/ace-1.3.3/assets/js/ace-extra.js"></script>

		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="/res/ace-1.3.3/assets/js/html5shiv.js"></script>
		<script src="/res/ace-1.3.3/assets/js/respond.js"></script>
		<![endif]-->
	</head>

	<body class="no-skin">
		<!-- #section:basics/navbar.layout -->
		<div id="navbar" class="navbar navbar-default navbar-fixed-top">
			<script type="text/javascript">
				try{ace.settings.check('navbar' , 'fixed')}catch(e){}
			</script>

			<div class="navbar-container" id="navbar-container">
				<!-- #section:basics/sidebar.mobile.toggle -->
				<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
					<span class="sr-only">Toggle sidebar</span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>
				</button>

				<!-- #section:basics/navbar.dropdown -->
				<jsp:include page="/WEB-INF/view/common/top.jsp" flush="true" />		

				<!-- /section:basics/navbar.dropdown -->
			</div><!-- /.navbar-container -->
		</div>

		<!-- /section:basics/navbar.layout -->
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<!-- #section:basics/sidebar -->
			<div id="sidebar" class="sidebar sidebar-fixed responsive">
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
				</script>

				<div class="sidebar-shortcuts" id="sidebar-shortcuts">
					<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
						<button class="btn btn-success">
							<i class="ace-icon fa fa-signal"></i>
						</button>

						<button class="btn btn-info">
							<i class="ace-icon fa fa-pencil"></i>
						</button>

						<!-- #section:basics/sidebar.layout.shortcuts -->
						<button class="btn btn-warning">
							<i class="ace-icon fa fa-users"></i>
						</button>

						<button class="btn btn-danger">
							<i class="ace-icon fa fa-cogs"></i>
						</button>

						<!-- /section:basics/sidebar.layout.shortcuts -->
					</div>

					<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
						<span class="btn btn-success"></span>

						<span class="btn btn-info"></span>

						<span class="btn btn-warning"></span>

						<span class="btn btn-danger"></span>
					</div>
				</div><!-- /.sidebar-shortcuts -->

				<!-- 菜单 -->
				<jc:menu />
				<!-- /.nav-list -->

				<!-- #section:basics/sidebar.layout.minimize -->
				<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
					<i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
				</div>

				<!-- /section:basics/sidebar.layout.minimize -->
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
				</script>
			</div>

			<!-- /section:basics/sidebar -->
			<div class="main-content">
				<div class="main-content-inner">
					<!-- #section:basics/content.breadcrumbs -->
					<div class="breadcrumbs breadcrumbs-fixed" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<!-- 面包屑 -->
					<jc:breadcrumb />
					</div>

					<!-- /section:basics/content.breadcrumbs -->
					<div class="page-content">

						<div class="row">
							<div class="col-xs-12">
							<!-- PAGE CONTENT BEGINS -->
							<div class="widget-box">
											<div class="widget-header widget-header-small">
												<h5 class="widget-title lighter">筛选</h5>
											</div>

											<div class="widget-body">
												<div class="widget-main">
														<div class="row">
															<div class="col-xs-12 col-sm-8">
																<div class="input-group">
																	<span class="input-group-addon">
																		<i class="ace-icon fa fa-check"></i>
																	</span>

																	<input type="text" id="name" name="name" class="form-control search-query" placeholder="请输入关键字" />
																	<span class="input-group-btn">
																		<button type="button" id="btn_search" class="btn btn-purple btn-sm">
																			<span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
																			搜索
																		</button>
																	</span>
																</div>
															</div>
														</div>
												</div>
											</div>
										</div>
							</div>
							<div class="col-xs-12">
								<div class="row-fluid" style="margin-bottom: 5px;">
									<div class="span12 control-group">
										<jc:button className="btn btn-primary" id="btn-add" textName="添加"/>
									</div>
								</div>
								<!-- PAGE CONTENT BEGINS -->
								<table id="grid-table"></table>

								<div id="grid-pager"></div>

								<script type="text/javascript">
									var $path_base = "..";//in Ace demo this will be used for editurl parameter
								</script>

								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
						
					</div>
				</div>
			</div><!-- /.main-content -->

			<jsp:include page="/WEB-INF/view/common/footer.jsp" flush="true" />

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script type="text/javascript">
			window.jQuery || document.write("<script src='/res/ace-1.3.3/assets/js/jquery.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='/res/ace-1.3.3/assets/js/jquery1x.js'>"+"<"+"/script>");
</script>
<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='/res/ace-1.3.3/assets/js/jquery.mobile.custom.js'>"+"<"+"/script>");
		</script>
		<script src="/res/ace-1.3.3/assets/js/bootstrap.js"></script>
	
		<script src="/res/ace-1.3.3/assets/js/date-time/bootstrap-datepicker.js"></script>
		<script src="/res/ace-1.3.3/assets/js/jqGrid/jquery.jqGrid.src.js"></script>
		<script src="/res/ace-1.3.3/assets/js/jqGrid/i18n/grid.locale-en.js"></script>
		
		<!-- ace scripts -->
		
		<!-- ace scripts -->
		<script src="/res/ace-1.3.3/assets/js/ace/elements.scroller.js"></script>
		<script src="/res/ace-1.3.3/assets/js/ace/elements.colorpicker.js"></script>
		<script src="/res/ace-1.3.3/assets/js/ace/elements.fileinput.js"></script>
		<script src="/res/ace-1.3.3/assets/js/ace/elements.typeahead.js"></script>
		<script src="/res/ace-1.3.3/assets/js/ace/elements.wysiwyg.js"></script>
		<script src="/res/ace-1.3.3/assets/js/ace/elements.spinner.js"></script>
		<script src="/res/ace-1.3.3/assets/js/ace/elements.treeview.js"></script>
		<script src="/res/ace-1.3.3/assets/js/ace/elements.wizard.js"></script>
		<script src="/res/ace-1.3.3/assets/js/ace/elements.aside.js"></script>
		<script src="/res/ace-1.3.3/assets/js/ace/ace.js"></script>
		<script src="/res/ace-1.3.3/assets/js/ace/ace.ajax-content.js"></script>
		<script src="/res/ace-1.3.3/assets/js/ace/ace.touch-drag.js"></script>
		<script src="/res/ace-1.3.3/assets/js/ace/ace.sidebar.js"></script>
		<script src="/res/ace-1.3.3/assets/js/ace/ace.sidebar-scroll-1.js"></script>
		<script src="/res/ace-1.3.3/assets/js/ace/ace.submenu-hover.js"></script>
		<script src="/res/ace-1.3.3/assets/js/ace/ace.widget-box.js"></script>
		<script src="/res/ace-1.3.3/assets/js/ace/ace.settings.js"></script>
		<script src="/res/ace-1.3.3/assets/js/ace/ace.settings-rtl.js"></script>
		<script src="/res/ace-1.3.3/assets/js/ace/ace.settings-skin.js"></script>
		<script src="/res/ace-1.3.3/assets/js/ace/ace.widget-on-reload.js"></script>
		<script src="/res/ace-1.3.3/assets/js/ace/ace.searchbox-autocomplete.js"></script>
		<script src="/res/js/layer/layer.js"></script>
		
		<script type="text/javascript"> 
        $(document).ready(function () {
        	var grid_selector = "#grid-table";
			var pager_selector = "#grid-pager";
        	//resize to fit page size
			$(window).on('resize.jqGrid', function () {
				$(grid_selector).jqGrid( 'setGridWidth', $(".page-content").width() );
		    });
//resize on sidebar collapse/expand
				var parent_column = $(grid_selector).closest('[class*="col-"]');
				$(document).on('settings.ace.jqGrid' , function(ev, event_name, collapsed) {
					if( event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed' ) {
						//setTimeout is for webkit only to give time for DOM changes and then redraw!!!
						setTimeout(function() {
							$(grid_selector).jqGrid( 'setGridWidth', parent_column.width() );
						}, 0);
					}
			    });

            $("#grid-table").jqGrid({
                url:'/mactivity/getListData',
                mtype: "GET",
                datatype: "json",
                colModel: [
                    { label: 'ID', name: 'id', key: true, width: 75 },
                    { label: '标题', name: 'title', width: 100 },
                    { label: '开始时间', name: 'start_time', width: 100 },
                    { label: '结束时间', name: 'end_time', width: 100 },
                    { label: '状态', name: 'status', formatter:fmatterStatus,width: 30 },
                    { label: '操作', name: 'id',formatter:fmatterOperation, width:120,sortable:false}
                ],
				viewrecords: true,
                height: 280,
                rowNum: 10,
                multiselect: false,//checkbox多选
                altRows: true,//隔行变色
                recordtext:"{0} - {1} 共 {2} 条",
                pgtext:"第 {0} 页 共 {1} 页",
                pager: pager_selector,
                loadComplete : function() {
					var table = this;
					setTimeout(function(){
						updatePagerIcons(table);
					}, 0);
				}
            });
			$(window).triggerHandler('resize.jqGrid');
			
			$("#btn_search").click(function(){  
			    //此处可以添加对查询数据的合法验证  
			    var name = $("#name").val();  
			    $("#grid-table").jqGrid('setGridParam',{  
			        datatype:'json',  
			        postData:{'name':name}, //发送数据  
			        page:1  
			    }).trigger("reloadGrid"); //重新载入  
			}); 
			
			$("#btn-visible").click(function(){
				setVisible(1);
			});
			$("#btn-unvisible").click(function(){
				setVisible(0);
			});
			
        });
      //replace icons with FontAwesome icons like above
		function updatePagerIcons(table) {
			var replacement = 
			{
				'ui-icon-seek-first' : 'ace-icon fa fa-angle-double-left bigger-140',
				'ui-icon-seek-prev' : 'ace-icon fa fa-angle-left bigger-140',
				'ui-icon-seek-next' : 'ace-icon fa fa-angle-right bigger-140',
				'ui-icon-seek-end' : 'ace-icon fa fa-angle-double-right bigger-140'
			};
			$('.ui-pg-table:not(.navtable) > tbody > tr > .ui-pg-button > .ui-icon').each(function(){
				var icon = $(this);
				var $class = $.trim(icon.attr('class').replace('ui-icon', ''));
				if($class in replacement) icon.attr('class', 'ui-icon '+replacement[$class]);
			})
		}
      	/**获取选中的列***/
		function getSelectedRows() {
            var grid = $("#grid-table");
            var rowKey = grid.getGridParam("selrow");
            if (!rowKey)
                return "-1";
            else {
                var selectedIDs = grid.getGridParam("selarrrow");
                var result = "";
                for (var i = 0; i < selectedIDs.length; i++) {
                    result += selectedIDs[i] + ",";
                }
                return result;
            }                
        }
      	function getOneSelectedRows() {
      		var grid = $("#grid-table");
            var rowKey = grid.getGridParam("selrow");
            if (!rowKey){
                return "-1";
            }else {
                var selectedIDs = grid.getGridParam("selarrrow");
                var result = "";
                /* for (var i = 0; i < selectedIDs.length; i++) {
                    result += selectedIDs[i] + ",";
                } */
                if(selectedIDs.length==1){
                	return selectedIDs[0];
                }else{
                	return "-2";
                }
            } 
      	}
      	function fmatterOperation(cellvalue, options, rowObject){
			return '<button class=\"btn btn-primary btn-sm\" onclick=\"activity_edit('+cellvalue+')\">编辑</button>&nbsp;<button class=\"btn btn-sm\" onclick=\"musics_edit('+cellvalue+')\">配置歌曲</button>&nbsp;<button class=\"btn btn-success btn-sm\" onclick=\"to_top_scroe('+cellvalue+')\">查看排名</button>';
		}
      	function fmatterStatus(cellvalue, options, rowObject){
      		if(cellvalue==0){
				return '<span class="label label-sm label-warning">关闭</span>';
			}else{
				return '<span class="label label-sm label-success">正常</span>';
			}
      	}
      	function activity_edit(id){
      		window.location.href="/mactivity/add?id="+id;
      	}
    	function musics_edit(id){
      		window.location.href="/mactivity/music_list?aid="+id;
      	}
		function setVisible(status){
			var submitData = {
					"bids" : getSelectedRows(),
					"visible":status
			};
			$.post("/sys/role/setVisible", submitData,function(data) {

				if (data.code == 0) {
					layer.msg("操作成功", {
					    icon: 1,
					    time: 1000 //1秒关闭（如果不配置，默认是3秒）
					},function(){
						//$("#grid-table").trigger("reloadGrid"); //重新载入
						reloadGrid();
					});
					  
				}  else{
					layer.alert("操作失败");
				} 
			},"json");
		}
		//格式化状态显示
		function fmatterStatus(cellvalue, options, rowObject){
			if(cellvalue==0){
				return '<span class="label label-sm label-warning">禁用</span>';
			}else{
				return '<span class="label label-sm label-success">启用</span>';
			}
		}
		function reloadGrid(){
			$("#grid-table").trigger("reloadGrid"); //重新载入
		}
   </script>
		<!-- the following scripts are used in demo only for onpage help and you don't need them -->
		
	</body>
</html>